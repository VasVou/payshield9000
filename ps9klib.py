import socket
import struct
import enum
import sys


class ParseError(Exception):
    pass


class CommandError(Exception):
    def __init__(self, value):
        self.error = value

    def __str__(self):
        return repr(self.error.text)

    def Code(self):
        return self.error.code


def MemDump(Title, data, size=16):
    from string import printable as PrintableString
    printable = bytes(PrintableString, 'ascii').replace(b'\r', b'').replace(b'\n', b'') \
        .replace(b'\t', b'').replace(b'\x0b', b'') \
        .replace(b'\x0c', b'')

    if (isinstance(data, bytes) == False):
        raise TypeError("Expected bytes for data")

    print("{} [{}]:".format(Title, len(data)))
    TheDump = ""

    for line in [data[i:i + size] for i in range(0, len(data), size)]:

        for c in line:
            TheDump += "{:02x} ".format(c)

        TheDump += "   " * (size - len(line))

        TheDump += ' | '
        for c in line:
            if c in printable:
                TheDump += "{:1c}".format(c)
            else:
                TheDump += '.'

        TheDump += "\n"

    print(TheDump)


class ErrorCode():
    ErrorCodes = {
        '00': 'No error',
        '01': 'Verification failure or warning of imported key parity error',
        '02': 'Key inappropriate length for algorithm',
        '04': 'Invalid key type code',
        '05': 'Invalid key length flag',
        '10': 'Source key parity error',
        '11': 'Destination key parity error or key all zeros',
        '12': 'Contents of user storage not available. Reset, power-down or overwrite',
        '13': 'Invalid LMK Identifier',
        '14': 'PIN encrypted under LMK pair 02-03 is invalid',
        '15': 'Invalid input data (invalid format, invalid characters, or not enough data provided)',
        '16': 'Console or printer not ready or not connected',
        '17': 'HSM not authorized, or operation prohibited by security settings',
        '18': 'Document format definition not loaded',
        '19': 'Specified Diebold Table is invalid',
        '20': 'PIN block does not contain valid values',
        '21': 'Invalid index value, or index/block count would cause an overflow condition',
        '22': 'Invalid account number',
        '23': 'Invalid PIN block format code. (Use includes where the security setting to implement PCI HSM limitations on PIN Block format usage is applied, and a Host command attempts to convert a PIN Block to a disallowed format.)',
        '24': 'PIN is fewer than 4 or more than 12 digits in length',
        '25': 'Decimalization Table error',
        '26': 'Invalid key scheme',
        '27': 'Incompatible key length',
        '28': 'Invalid key type',
        '29': 'Key function not permitted',
        '30': 'Invalid reference number',
        '31': 'Insufficient solicitation entries for batch',
        '32': 'LIC007 (AES) not installed',
        '33': 'LMK key change storage is corrupted',
        '39': 'Fraud detection',
        '40': 'Invalid checksum',
        '41': 'Internal hardware/software error: bad RAM, invalid error codes, etc.',
        '42': 'DES failure',
        '43': 'RSA Key Generation Failure',
        '47': 'Algorithm not licensed',
        '49': 'Private key error, report to supervisor',
        '51': 'Invalid message header',
        '65': 'Transaction Key Scheme set to None',
        '67': 'Command not licensed',
        '68': 'Command has been disabled',
        '69': 'PIN block format has been disabled',
        '74': 'Invalid digest info syntax (no hash mode only)',
        '75': 'Single length key masquerading as double or triple length key',
        '76': 'Public key length error',
        '77': 'Clear data block error',
        '78': 'Private key length error',
        '79': 'Hash algorithm object identifier error',
        '80': 'Data length error. The amount of MAC data (or other data) is greater than or less than the expected amount.',
        '81': 'Invalid certificate header',
        '82': 'Invalid check value length',
        '83': 'Key block format error',
        '84': 'Key block check value error',
        '85': 'Invalid OAEP Mask Generation Function',
        '86': 'Invalid OAEP MGF Hash Function',
        '87': 'OAEP Parameter Error',
        '90': 'Data parity error in the request message received by the HSM',
        '91': 'Longitudinal Redundancy Check (LRC) character does not match the value computed over the input data (when the HSM has received a transparent async packet)',
        '92': 'The Count value (for the Command/Data field) is not between limits, or is not correct (when the HSM has received a transparent async packet)',
        'A1': 'Incompatible LMK schemes',
        'A2': 'Incompatible LMK identifiers',
        'A3': 'Incompatible keyblock LMK identifiers',
        'A4': 'Key block authentication failure',
        'A5': 'Incompatible key length',
        'A6': 'Invalid key usage',
        'A7': 'Invalid algorithm',
        'A8': 'Invalid mode of use',
        'A9': 'Invalid key version number',
        'AA': 'Invalid export field',
        'AB': 'Invalid number of optional blocks',
        'AC': 'Optional header block error',
        'AD': 'Key status optional block error',
        'AE': 'Invalid start date/time',
        'AF': 'Invalid end date/time',
        'B0': 'Invalid encryption mode',
        'B1': 'Invalid authentication mode',
        'B2': 'Miscellaneous keyblock error',
        'B3': 'Invalid number of optional blocks',
        'B4': 'Optional block data error',
        'B5': 'Incompatible components',
        'B6': 'Incompatible key status optional blocks',
        'B7': 'Invalid change field',
        'B8': 'Invalid old value',
        'B9': 'Invalid new value',
        'BA': 'No key status block in the keyblock',
        'BB': 'Invalid wrapping key',
        'BC': 'Repeated optional block',
        'BD': 'Incompatible key types',
        'BE': 'Invalid keyblock header ID'
    }

    def __init__(self, code):
        self.code = code
        self.text = self.ErrorCodes[self.code]

    def __repr__(self):
        return self.code


class KeySchemes(enum.Enum):
    Variant2DES = 'U'
    Variant3DES = 'T'
    X917Double = 'X'
    X917Triple = 'Y'
    GISKE = 'V'
    TR31 = 'R'
    THKeyBlock = 'S'


class KeyClass():
    def __init__(self, KeyData):
        self._KeyData = KeyData
        self._ParseKeyData()

    def KeyLength(self):
        return len(self._KeyData)

    def Data(self):
        return self._KeyData

    def Scheme(self):
        return self._KeyScheme

    def Usage(self):
        return self._Usage

    def _ParseKeyData(self):
        raise NotImplementedError


class X917():
    class Key(KeyClass):
        def _ParseKeyData(self):
            if self._KeyData[0] == KeySchemes.X917Double.value:
                self._KeyScheme = KeySchemes.X917Double.value
                Length = 32
            elif self._KeyData[0] == KeySchemes.X917Triple.value:
                self._KeyScheme = KeySchemes.X917Triple.value
                Length = 48
            else:
                raise ParseError('Invalid key scheme')

            if len(self._KeyData) < Length + 1:
                raise ParseError('Key has invalid length')

            self._KeyData = self._KeyData[:Length + 1]


class KeyBlock():
    class Algorithm(enum.Enum):
        AES = 'A'
        DES = 'D'
        EC = 'E'
        HMAC = 'H'
        RSA = 'R'
        DSA = 'S'
        TDES = 'T'

        SDES = 'D1'
        TDES2 = 'T2'
        TDES3 = 'T3'
        AES128 = 'A1'
        AES192 = 'A2'
        AES256 = 'A3'

    class ModeOfUse(enum.Enum):
        Decryption = 'D'
        Encryption = 'E'
        EncryptionDecryption = 'B'

        MACGeneration = 'G'
        MAC = 'C'

        SignatureGeneration = 'S'
        SignatureVerification = 'V'

        KeyDerivation = 'X'

        All = 'N'

    class Export(enum.Enum):
        Trusted = 'E'
        No = 'N'
        Sensitive = 'S'


class THKeyBlock(KeyBlock):
    class Usage(enum.Enum):
        WWK = '01'
        RSAPublic = '02'
        RSAPrivate = '03'
        BDK1 = 'B0'
        BDK2 = '41'
        BDK3 = '42'
        IKEY = 'B1'
        CVK = 'C0'
        CSC = '11'  # amex
        CVC = '12'  # mastercard
        CVV = '13'  # visa
        DEKGen = 'D0'
        DEK = '21'
        ZEK = '22'
        TEK = '23'
        MKAC = 'E0'
        MKSMC = 'E1'
        MKSMI = 'E2'
        MKDAC = 'E3'
        MKDN = 'E4'
        MKCardPerso = 'E5'
        MKOther = 'E6'
        KML = '31'
        MKCVC3 = '32'
        KEK = 'K0'
        TMK = '51'
        ZMK = '52'
        ZKA = '53'
        MAC16609 = 'M0'
        MAC9797_1 = 'M1'
        MAC9797_2 = 'M2'
        MAC9797_3 = 'M3'
        MAC9797_4 = 'M4'
        MACcbc = 'M5'
        MACcmac = 'M6'
        HMACsha1 = '61'
        HMACsha224 = '62'
        HMACsha256 = '63'
        HMACsha384 = '64'
        HMACsha512 = '65'
        PEKGeneric = 'P0'
        TPK = '71'
        ZPK = '72'
        TKR = '73'
        PVK = 'V0'
        PVKibm = 'V1'
        PVV = 'V2'
        EMVSesKeyCryptograms = '47'

    class Key(KeyClass):

        def _ParseKeyData(self):
            if self._KeyData[0] is KeySchemes.THKeyBlock.value:
                self._KeyScheme = KeySchemes.THKeyBlock
            else:
                raise ParseError

            self._VersionID = int(self._KeyData[1])

            self._BlockLength = int(self._KeyData[2:6])

            # After parsing the actual length, crop the end of the raw data
            self._KeyData = self._KeyData[:self._BlockLength + 1]

            self._Usage = THKeyBlock.Usage(self._KeyData[6:8])

            self._Algorithm = KeyBlock.Algorithm(self._KeyData[8])

            self._ModeOfUse = KeyBlock.ModeOfUse(self._KeyData[9])

            self._Version = int(self._KeyData[10:12])

            self._Export = KeyBlock.Export(self._KeyData[12])

            self._OptionalBlocks = int(self._KeyData[13:15])

            # TODO: Support optional blocks
            if int(self._OptionalBlocks) > 0:
                raise NotImplementedError

            try:
                self._LMKID = int(self._KeyData[15:17])
            except ValueError:  # case of ZMK/TMK keyblock
                self._LMKID = None

        def _ParseTR31KeyData(self):
            self._VersionID = str(self._KeyData[1])

            self._BlockLength = int(self._KeyData[2:6])

            # After parsing the actual length, crop the end of the raw data
            self._KeyData = self._KeyData[:self._BlockLength + 1]

            self._Usage = KeyBlock.TR31Usage(self._KeyData[6:8])

            self._Algorithm = KeyBlock.Algorithm(self._KeyData[8])

            self._ModeOfUse = KeyBlock.ModeOfUse(self._KeyData[9])

            self._Version = int(self._KeyData[10:12])

            self._Export = KeyBlock.Export(self._KeyData[12])

            self._OptionalBlocks = int(self._KeyData[13:15])

            # TODO: Support optional blocks
            if int(self._OptionalBlocks) > 0:
                raise NotImplementedError


class TR31(KeyBlock):
    class Usage(enum.Enum):
        BDK1 = 'B0'
        BDK2 = 'B0'
        BDK3 = 'B0'
        BDK4 = 'B0'
        IKEY = 'B1'

        CVK = 'C0'
        CSC = 'C0'  # amex
        CVC = 'C0'  # mastercard
        CVV = 'C0'  # visa

        DEKGen = 'D0'
        DEK = 'D0'
        ZEK = 'D0'
        TEK = 'D0'

        MKAC = 'E0'
        MKSMC = 'E1'
        MKSMI = 'E2'
        MKDAC = 'E3'
        MKDN = 'E4'
        MKCardPerso = 'E5'
        MKOther = 'E6'
        KML = 'E6'
        MKCVC3 = 'E6'

        MS_KEY_MAC = 'M3'

        InitVal = 'I0'

        KEK = 'K0'
        TMK = 'K0'
        ZMK = 'K0'

        MAC16609 = 'M0'
        MAC9797_1 = 'M1'
        MAC9797_2 = 'M2'
        MAC9797_3 = 'M3'
        MAC9797_4 = 'M4'
        MACcmac = 'M5'

        PEKGeneric = 'P0'
        TPK = 'P0'
        ZPK = 'P0'
        TKR = 'P0'

        PVK = 'V0'
        PVKibm = 'V1'
        PVV = 'V2'

    class Key(KeyClass):
        def _ParseKeyData(self):

            if self._KeyData[0] is KeySchemes.TR31.value:
                self._KeyScheme = KeySchemes.TR31
            else:
                raise ParseError

            self._VersionID = str(self._KeyData[1])

            self._BlockLength = int(self._KeyData[2:6])

            # After parsing the actual length, crop the end of the raw data
            self._KeyData = self._KeyData[:self._BlockLength + 1]

            self._Usage = TR31.Usage(self._KeyData[6:8])

            self._Algorithm = KeyBlock.Algorithm(self._KeyData[8])

            self._ModeOfUse = KeyBlock.ModeOfUse(self._KeyData[9])

            self._Version = int(self._KeyData[10:12])

            self._Export = KeyBlock.Export(self._KeyData[12])

            self._OptionalBlocks = int(self._KeyData[13:15])

            # TODO: Support optional blocks
            if int(self._OptionalBlocks) > 0:
                raise NotImplementedError


class Variant():
    class KeyType():
        ZMK = '000'
        KML = '200'
        ZPK = '001'
        PVK = '002'
        TMK = '002'
        TPK = '002'
        IKEY = '302'
        CVK = '402'
        CSCK = '402'
        TAK = '003'
        WWK = '006'
        ZAK = '008'
        BDK1 = '009'
        BDK2 = '609'
        BDK3 = '809'
        MK_AC = '109'
        MK_SMI = '209'
        MK_SMC = '309'
        MK_DAC = '409'
        MK_DN = '509'
        MK_CVC3 = '709'
        ZEK = '00A'
        DEK = '00B'
        TEK = '30B'
        KEK = '107'
        # ...
        BKEM = '506'
        BKAM = '606'
        # No Operation KeyType. Doesn't exist in the specs, just using it internally
        NOP = 'FFF'

    class Key(KeyClass):

        def _ParseKeyData(self):
            if self._KeyData[0] == KeySchemes.Variant2DES.value:
                self._KeyScheme = KeySchemes.Variant2DES.value
                Length = 32
            elif self._KeyData[0] == KeySchemes.Variant3DES.value:
                self._KeyScheme = KeySchemes.Variant3DES.value
                Length = 48
            else:
                raise ParseError

            self._KeyData = self._KeyData[:Length + 1]


class HSM():

    def __init__(self, ConnectionInfo, Header='0000', AutoConnect=False, Debug=False, LMKType=KeySchemes.Variant2DES):
        self.ConnectionInfo = ConnectionInfo
        self.Debug = Debug
        self.Header = Header
        self.AutoConnect = AutoConnect

        self.LMKType = LMKType

        if self.LMKType not in (KeySchemes.Variant2DES, KeySchemes.THKeyBlock):
            raise ParseError

        self.socket = None
        self._Connecting = False

    def Connect(self):
        if self.__IsConnected() == True:
            return False

        self._Connecting = True
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect(self.ConnectionInfo)
            self._Connecting = False
        except:
            self.socket = None
            self._Connecting = False
            raise

    def Disconnect(self):
        if self.__IsConnected() == False:
            return False

        self.socket.close()
        self.socket = None

    def __IsConnected(self):
        from time import sleep
        while self._Connecting == True:
            sleep(0.1)

        return (self.socket != None)

    def __PackRequest(self, Command, Data):
        msg = self.Header.encode() + Command.encode() + Data.encode()
        return struct.pack("!H", len(msg)) + msg

    def __UnpackResponse(self, Data):
        reply = Data[2 + len(self.Header):]

        CmdResponse = reply[:2].decode()
        CmdResponseCode = ErrorCode(reply[2:4].decode())
        try:
            CmdData = reply[4:].decode()
        except:
            CmdData = reply[4:]

        return (CmdResponse, CmdResponseCode, CmdData)

    def __ExecuteCommand(self, Command, Data=''):
        if self.AutoConnect == True and self.__IsConnected() == False:
            self.Connect()

        data = self.__PackRequest(Command, Data)

        if self.Debug == True:
            MemDump('Sending:', data)

        self.socket.send(data)

        data = self.socket.recv(4096)

        if self.Debug == True:
            MemDump('Received:', data)

        return self.__UnpackResponse(data)

    def Diagnostics(self):
        CmdResponse, CmdResponseCode, CmdData = self.__ExecuteCommand('NC')

        LMKCheck = CmdData[:16]
        FWVersion = CmdData[16:]

        return CmdResponseCode, LMKCheck, FWVersion

    # Modes:
    # 0 : Generate key
    # 1 : Generate and encrypt under ZMK/TMK
    # A : Derive key
    # B : Derive and encrypt under ZMK/TMK

    def GenerateKey(self, KeyType='FFF', Mode='0', KeyScheme=None,

                    # Derivation specific
                    DeriveKeyMode=None,
                    DukptMasterKeyType=None, DukptMasterKey=None, KSN=None,
                    ZKAMasterKeyType='FFF', ZKAMasterKey=None, ZKAOption=1, ZKARNDI=None,

                    # Export under ZMK/TMK specific
                    EncKeyFlag='0', EncKey=None, EncKeyScheme=KeySchemes.Variant2DES,

                    # keyblock LMK specific
                    KeyUsage=None, Algorithm=None, ModeOfUse=KeyBlock.ModeOfUse.All, KeyVersion='00',
                    Exportability=KeyBlock.Export.No, OptionalBlocks='00',

                    # TR31 Specific
                    ModifiedExport = None
                    ):

        if KeyScheme == None:
            KeyScheme = self.LMKType

        data = Mode + KeyType + KeyScheme.value

        if Mode == '1':
            data += ';' + EncKeyFlag + EncKey.Data() + EncKeyScheme.value

        if KeyScheme == KeySchemes.THKeyBlock:
            KeyBlockData = '#' + KeyUsage.value + Algorithm.value + ModeOfUse.value + KeyVersion + Exportability.value + OptionalBlocks

            data += KeyBlockData

        if EncKeyScheme == KeySchemes.TR31 and ModifiedExport is not None:
            data += '&' + ModifiedExport.value

        CmdData = self.__ExecuteCommand('A0', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)

        if KeyScheme == KeySchemes.THKeyBlock and CmdData[1].code == '00':
            p = 0

            KeyUnderLMK = KeyBlock.Key(CmdData[2])
            p += KeyUnderLMK.KeyLength()

            AnswerData += (KeyUnderLMK,)

            if Mode == '1':
                KeyUnderEnc = KeyBlock.Key(CmdData[2][p:])
                AnswerData += (KeyUnderEnc,)
                p += KeyUnderEnc.KeyLength()

            AnswerData += (CmdData[2][p:],)

        return AnswerData

    def ImportKey(self, EncKey, Key, KeyType='FFF', KeyScheme=None,

                  # keyblock LMK specific
                  KeyUsage=None, ModeOfUse=KeyBlock.ModeOfUse.All, KeyVersion='00',
                  Exportability=KeyBlock.Export.No
                  ):

        if KeyScheme == None:
            KeyScheme = self.LMKType

        data = KeyType + EncKey.Data() + Key.Data() + KeyScheme.value

        if Key.Scheme() is KeySchemes.TR31 and self.LMKType is KeySchemes.THKeyBlock:
            data += '#' + KeyUsage.value + '00'

        elif KeyScheme == KeySchemes.THKeyBlock:
            data += '#' + KeyUsage.value + ModeOfUse.value + KeyVersion + Exportability.value + '00'

        CmdData = self.__ExecuteCommand('A6', data)

        if CmdData[1].code != '00' and CmdData[1].code != '01':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)
        p = 0

        if CmdData[1].code == '00' or CmdData[1].code == '01':
            if KeyScheme == KeySchemes.THKeyBlock:
                ExportedKey = KeyBlock.Key(CmdData[2])
            elif KeyScheme == KeySchemes.Variant2DES or KeyScheme == KeySchemes.Variant3DES:
                ExportedKey = Variant.Key(CmdData[2])
            elif KeyScheme == KeySchemes.X917Double or KeyScheme == KeySchemes.X917Triple:
                ExportedKey = X917.Key(CmdData[2])

            p += ExportedKey.KeyLength()

            AnswerData += (ExportedKey,)
            AnswerData += (CmdData[2][p:],)

        return AnswerData

    def ExportKey(self, EncKey, Key, KeyType='FFF', KeyScheme=None):

        if KeyScheme == None:
            KeyScheme = self.LMKType

        data = KeyType + EncKey.Data() + Key.Data() + KeyScheme.value

        CmdData = self.__ExecuteCommand('A8', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)
        p = 0

        if CmdData[1].code == '00':

            if KeyScheme == KeySchemes.THKeyBlock:
                ExportedKey = KeyBlock.Key(CmdData[2])
            elif KeyScheme == KeySchemes.Variant2DES or KeyScheme == KeySchemes.Variant3DES:
                ExportedKey = Variant.Key(CmdData[2])
            elif KeyScheme == KeySchemes.X917Double or KeyScheme == KeySchemes.X917Triple:
                ExportedKey = X917.Key(CmdData[2])

            p += ExportedKey.KeyLength()

            AnswerData += (ExportedKey,)

        AnswerData += (CmdData[2][p:],)

        return AnswerData

    def KeyCheckValue(self, Key, KeyType='FFF', KeyLength='F', KeyTypeCode='FF', KeyScheme=None):

        if KeyScheme == None:
            KeyScheme = self.LMKType

        data = KeyTypeCode + KeyLength + Key.Data()

        if KeyTypeCode == 'FF':
            data += ';' + KeyType

        CmdData = self.__ExecuteCommand('BU', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)
        p = 0

        if CmdData[1].code == '00':
            AnswerData += (CmdData[2][:6],)

        return AnswerData

    # Format
    # 0 : Binary
    # 1 : Hex-Encoded Binary
    # 2 : Text

    # Size
    # 0 : 8  hex digits
    # 1 : 16 hex digits

    # Algo
    # 1 : ISO 9797 MAC algorithm 1 (ANSI X9.9)
    # 3 : ISO 9797 MAC algorithm 3 (ANSI X9.19)
    # 5 : CBC-MAC
    # 6 : CMAC

    # Padding
    # 0 : No padding
    # 1 : ISO 9797 Padding method 1
    # 2 : ISO 9797 Padding method 2
    # 3 : ISO 9797 Padding method 3
    # 4 : AES CMAC Padding

    def GenerateMAC(self, Key, Data, KeyType='FFF', Format='0', Size='0', Algo='0', Padding='0'):

        # TODO: Fix for longer than 32k buffers
        data = '0' + Format + Size + Algo + Padding + KeyType + Key.Data() + '{:>04X}'.format(len(Data)) + Data

        CmdData = self.__ExecuteCommand('M6', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)
        p = 0

        if CmdData[1].code == '00':
            AnswerData += (CmdData[2][:8],) if Size == 0 else (CmdData[2][:16],)

        return AnswerData

    def VerifyMAC(self, Key, Data, MAC, KeyType='FFF', Format='0', Size='0', Algo='0', Padding='0'):

        # TODO: Fix for longer than 32k buffers
        data = '0' + Format + Size + Algo + Padding + KeyType + Key.Data() + '{:>04X}'.format(len(Data)) + Data + MAC

        CmdData = self.__ExecuteCommand('M8', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)
        p = 0

        return AnswerData


    def Status(self, Mode='00'):

        if not (Mode == '00' or Mode == '01'):
            raise NotImplementedError(f'Mode:{Mode} Unsupported, Check Manual')

        CmdData = self.__ExecuteCommand('NO', Mode)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)

        if Mode is '00':
            p = 0
            buffer = CmdData[2][p:p + 1]
            AnswerData += (buffer,)
            p += 1
            ethType = CmdData[2][p:p + 1]
            AnswerData += (ethType,)
            p += 1
            TCPSockNum = CmdData[2][p:p + 2]
            AnswerData += (TCPSockNum,)
            p += 2
            FirmNum = CmdData[2][p:p + 9]
            AnswerData += (FirmNum,)
            p += 9
            Reserved1 = CmdData[2][p:p + 1]
            AnswerData += (Reserved1,)
            p += 1
            Reserved2 = CmdData[2][p:p + 4]
            AnswerData += (Reserved2,)

            return AnswerData

        if Mode is '01':
            p = 0
            AnswerData += (CmdData[2][p:p + 1],)
            p += 1
            AnswerData += (CmdData[2][p:p + 10],)

            return AnswerData

    def CustomeExec(self, cm, data):

        CmdData = self.__ExecuteCommand(cm, data)
        #
        # if CmdData[1].code != '00':
        #     raise CommandError(CmdData[1])

        # AnswerData = CommandError(CmdData[1])

        return CmdData

    def EncryptDataBlock(self, ModeFlag='00',

                         # ModeFlag = 11 specific
                         FPERadixValue=None, FPETweak=None,

                         InputFormatFlag=None, OutputFormatFlag=None, KeyType=None, Key=None,
                         KSNDescriptor=None, KSN=None, IV=None, OFBModeFlag=None,

                         # ModeFlag != 04,13
                         Message=None,

                         # ModeFlag = 04, 13
                         BlockType=None, DataBlock: list = [],
                         ):

        if ModeFlag not in ['00', '01', '02', '03', '04', '05', '11', '13']:
            raise ValueError(f'Invalid Mode Flag:{ModeFlag}')

        Data = ModeFlag

        if ModeFlag is '11':
            if FPERadixValue is not None:
                # if FPERadixValue not in range(00002,00257):
                FPERadixFlag = 'U'
            else:
                FPERadixFlag = 'A'

            FPETweakLength = len(FPETweak)

            Data += FPERadixFlag + FPERadixValue + FPETweakLength + FPETweak

        if ModeFlag is '04' or ModeFlag is '13':
            InputFormatFlag = ''
        else:
            if InputFormatFlag not in ['0', '1', '2']:
                raise ValueError(f'Invalid Input Format Flag:{InputFormatFlag}')

        if ModeFlag is '13':
            OutputFormatFlag = ''
        else:
            if OutputFormatFlag not in ['0', '1']:
                raise ValueError(f'Invalid Output Format Flag:{OutputFormatFlag}')
        # FIXME: Needs fixes
        if self.LMKType in [KeySchemes.Variant2DES, KeySchemes.Variant3DES]:
            if KeyType in [Variant.KeyType.BDK1, Variant.KeyType.BDK2, Variant.KeyType.BDK3, Variant.KeyType.BDK4]:
                if KSNDescriptor is None or KSN is None:
                    raise ValueError(f'{KSNDescriptor}:{KSN}')
            elif KeyType in [Variant.KeyType.DEK, Variant.KeyType.TEK]:
                KSNDescriptor = ''
                KSN = ''
            else:
                raise ValueError(f'Unsupported Key Type: {KeyType}')

        if self.LMKType in [KeySchemes.THKeyBlock]:
            KeyType = 'FFF'
            if Key.Usage() in [KeyBlock.Usage.BDK1, KeyBlock.Usage.BDK2, KeyBlock.Usage.BDK3]:
                if KSNDescriptor is None or KSN is None:
                    raise ValueError(f'{KSNDescriptor}:{KSN}')
            elif Key.Usage() in [KeyBlock.Usage.DEK, KeyBlock.Usage.TEK]:
                KSNDescriptor = ''
                KSN = ''
            else:
                raise ValueError(f'Unsupported Key Usage: {Key.Usage()}')

        if ModeFlag in ['01', '02', '02', '05']:
            if len(IV) is not (16 or 32):
                raise ValueError(f'IV len:{len(IV)}')
        else:
            IV = ''
        if ModeFlag is '05':
            if OFBModeFlag is not ('1' or '8'):
                raise ValueError(f'OFB Mode Flag:{OFBModeFlag}')
        else:
            OFBModeFlag = ''

        Data += InputFormatFlag + OutputFormatFlag + KeyType + Key.Data() + KSNDescriptor + KSN + IV + OFBModeFlag

        if ModeFlag in ['04', '13']:
            MessageLength = ''
            Message = ''
            BlockCount = len(DataBlock)
            tmpDataBlock = ''
            for i in DataBlock:
                tmpDataBlock += DataBlock[i]
            dt = len(DataBlock) + BlockType + len(tmpDataBlock) + tmpDataBlock
        else:
            MessageLength = f'{len(Message):>04X}'
            dt = ''

        Data += MessageLength + Message + dt

        CmdData = self.__ExecuteCommand('M0', Data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)

        if ModeFlag is not ('04' or '13'):
            AnswerData += (CmdData[2][:4],)
            if OutputFormatFlag is '0':
                AnswerData += (CmdData[2][4:int(CmdData[2][:4], 16)],)
            elif OutputFormatFlag is '1':
                AnswerData += (CmdData[2][4:int(CmdData[2][:4], 16)],)
        return AnswerData

    def GenerateRandomPIN(self, PAN: str = '1234567890123456', PINLen: int = 4):

        if PINLen not in range(4, 13):
            raise ValueError(f'PIN Len:{PINLen} not in range')

        data = f'{PAN[-13:-1]}' + f'{PINLen:02}'
        # TODO: Add PIN Exclude
        CmdData = self.__ExecuteCommand('JA', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        return CmdData

    def TranslatePIN_LMKtoZPK(self, Key, PINBlockFormat, PAN: str, PIN):

        if PINBlockFormat.value is '48':
            PAN = f'{PAN[:-1]};'
        elif PINBlockFormat.value is '04':
            PAN = f'{PAN[:-1]:F>18}'
        else:
            PAN = f'{PAN[-13:-1]}'

        data = Key.Data() + PINBlockFormat.value + PAN + PIN

        CmdData = self.__ExecuteCommand('JG', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1],)
        AnswerData += (CmdData[2],)

        return AnswerData

    def TranslatePIN_ZPKtoZPK(self, SourceZPK, DestZPK, SourcePB, SourcePBformat, DestPBformat, PAN):

        MaxPINLen = '12'

        if SourcePBformat is '48' or DestPBformat is '48':
            PAN = f'{PAN[:-1]};'
        elif DestPBformat is '04':
            PAN = f'{PAN[:-1]:F>18}'
        else:
            PAN = f'{PAN[-13:-1]}'

        data = SourceZPK.Data() + DestZPK.Data() + MaxPINLen + SourcePB + SourcePBformat.value + DestPBformat.value + PAN

        CmdData = self.__ExecuteCommand('CC', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        AnswerData = (CmdData[1], CmdData[2][:2], CmdData[2][2:],)

        return AnswerData


    def TranslatePIN_TPKtoZPK(self, SourceTPK, DestKey, MaxPinLen, SourcePB, SourcePBformat, DestPBformat, PAN,
                              DestKeyFlag=None, DestKSNDescriptor=None, DestKSN=None):

        data = SourceTPK.Data()

        if self.LMKType in [KeySchemes.Variant2DES, KeySchemes.Variant3DES]:
            if DestKeyFlag is Variant.KeyType.BDK1:
                data += '*'
            elif DestKeyFlag is Variant.KeyType.BDK2:
                data += '~'
            data += DestKey
            if DestKeyFlag in [Variant.KeyType.BDK1, Variant.KeyType.BDK2]:
                data += DestKSNDescriptor + DestKSN
        elif self.LMKType in [KeySchemes.THKeyBlock]:
            data += DestKey.Data()
            if DestKey.Usage() in [KeyBlock.Usage.BDK1, KeyBlock.Usage.BDK2]:
                data += DestKSNDescriptor + DestKSN

        if SourcePBformat is '48' or DestPBformat is '48':
            PAN = f'{PAN[:-1]};'
        elif DestPBformat is '04':
            PAN = f'{PAN[:-1]:F>18}'
        else:
            PAN = f'{PAN[-13:-1]}'

        data += MaxPinLen + SourcePB + SourcePBformat.value + DestPBformat.value + PAN

        CmdData = self.__ExecuteCommand('CA', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        return CmdData


    def TranslateKey_Migration(self, Key, KeyTypeCode_2d = 'FF', KeyLengthFlag = 'F', KeyTypeCode_3d = 'FFF'):

        data = KeyTypeCode_2d + KeyLengthFlag + Key + ';' + KeyTypeCode_3d

        data += '#' + '52N00S00'
        CmdData = self.__ExecuteCommand('BW', data)

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])

        return CmdData


    def HSMLoading(self):

        CmdData = self.__ExecuteCommand('J2', '')

        if CmdData[1].code != '00':
            raise CommandError(CmdData[1])
        import re

        rowdata = re.match('''
                    ([0-9A-Z] {12} ) # Serial Number
                    ([0-9A-F] {6}  ) # Start Date
                    ([0-9]    {6}  ) # Start Time
                    ([0-9A-F] {6}  ) # End Date
                    ([0-9]    {6}  ) # End Time
                    ([0-9A-F] {6}  ) # Current Date
                    ([0-9A-F] {6}  ) # Current Time
                    ([0-9]    {10} ) # Seconds
                ''', CmdData[2][:58], re.IGNORECASE | re.VERBOSE)

        rest = ()
        for entry in CmdData[2][58:].strip(';').split(';'):
            reqdata = re.match('''
                        ([0-9] {3} ) # Starting Percentage
                        ([0-9] {3} ) # Ending Percentage
                        ([0-9] {10}) # Number Time Periods
                    ''', entry, re.IGNORECASE | re.VERBOSE)

            rest += (reqdata.groups(),)

        return rowdata.groups(), rest